package com.kn.practice.sorting;

import java.util.Arrays;

public class BubbleSort {
//    We repeatedly compare each pair of adjacent items and swap them if they are in the wrong order.
//    In place, stable, O(N^2) time complexity, O(1) space complexity.
    public static void main(String[] args) {
        int items[] = {7, 4, 1, 9, 3, 2, 6};

        for (int i = 0; i < items.length; i++) {
            for (int j = 0; j < items.length - i - 1; j++) {
                if (items[j] > items[j + 1]) {
                    int temp;
                    temp = items[j];
                    items[j] = items[j + 1];
                    items[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(items));
    }
}
