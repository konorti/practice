package com.kn.practice.sorting;

import java.util.Arrays;

public class SelectionSort {
    //    We repeatedly find the minimum element and move it to the shorted part of the array to make the
    //    unsorted part sorted. When we find the min element we swap it with the first item in the unsorted
    //    part of the array.
    //    In place, stable, O(N^2) time complexity, O(1) space complexity.
    public static void main(String[] args) {
        int items[] = {3, 4, 7, 9, 1, 6, 2};
        for (int i = 0; i < items.length; i++) {
            int minIndex = i;
            for (int j = i; j < items.length; j++) {
                if (items[j] < items[minIndex]) {
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                int temp;
                temp = items[i];
                items[i] = items[minIndex];
                items[minIndex] = temp;
            }
        }
        System.out.println(Arrays.toString(items));
    }
}
