package com.kn.practice.sorting;

import java.util.Arrays;

public class InsertionSort {
//  Divide the array into two parts a sorted and an unsorted.
//  Remove the first element from the unsorted part and insert into
//  the correct position in the sorted part.
//  In place, stable, O(N^2) time complexity, O(1) space complexity.

    public static void main(String[] args) {
        int items[] = {4, 8, 3, 5, 1, 9, 2};
        for (int i = 1; i < items.length; i++) {
            int j = i;
            int temp = items[i];
            while (j > 0 && items[j - 1] > temp) {
                items[j] = items[j - 1];
                j--;
            }
            items[j] = temp;
        }
        System.out.println(Arrays.toString(items));
    }
}
