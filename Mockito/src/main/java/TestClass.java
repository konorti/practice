public class TestClass {
    private String privateField;

    private void privateMethod() {
        System.out.println("Private method of TestClass");
    }

    public String publicMethod() {
        return "Public method of TestClass";
    }

    public String publicMethod2() {
        return "Public method 2 of TestClass";
    }
}
