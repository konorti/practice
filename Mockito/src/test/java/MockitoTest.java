import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MockitoTest {

    @Mock
    TestClass mockedClass = new TestClass();

    @Spy
    TestClass spiedClass = new TestClass();

    @Test
    public void simpleMockTestCase() {
        //Mock a method call
        Mockito.when(mockedClass.publicMethod()).thenReturn("MockValue");
//        This would produce null since in case of mocks all methods are replaced to dummy mock ones.
//        System.out.println(testClass.publicMethod2());
        String actual = mockedClass.publicMethod();
        String expected = "MockValue";
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void simpleSpyTestCase() {
        Mockito.when(spiedClass.publicMethod()).thenReturn("MockValue");
        spiedClass.publicMethod();
        //Check if the public method has been called on the spiedClass.
        Mockito.verify(spiedClass).publicMethod();

//        This would fail since the publicMethod2 has not been called.
//        Mockito.verify(spiedClass).publicMethod2();

//        In contrast to mocks, spied objects contain real implementation even if some methods
//        are mocked. So this method will produce an output instead of null.
        System.out.println(spiedClass.publicMethod2());

        String expected = "MockValue";
        String actual = spiedClass.publicMethod();
        Assert.assertEquals(actual, expected);
    }

}
