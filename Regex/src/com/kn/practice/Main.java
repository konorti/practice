package com.kn.practice;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

        /**
         *   Regex quantifiers
         */

//        Greedy
//        X?
//        X+
//        X*
//        X{n}
//        X{n,}
//        X{n,m}
//
//        Reluctant
//        X??
//        X+?
//        X*?
//        X{n}?
//        X{n,}?
//        X{n,m}?
//
//        Possessive
//        X?+
//        X++
//        X*+
//        X{n}+
//        X{n,}+
//        X{n,m}+

        /**
         *   Regex predefined character classes
         */

//        .	    Any character (may or may not match line terminators)
//        \d	A digit: [0-9]
//        \D	A non-digit: [^0-9]
//        \s	A whitespace character: [ \t\n\x0B\f\r]
//        \S	A non-whitespace character: [^\s]
//        \w	A word character: [a-zA-Z_0-9]
//        \W	A non-word character: [^\w]

        /**
         *   Boundary matchers
         */

//        ^	    The beginning of a line
//        $	    The end of a line
//        \b	A word boundary
//        \B	A non-word boundary
//        \A	The beginning of the input
//        \G	The end of the previous match
//        \Z	The end of the input but for the final terminator, if any
//        \z	The end of the input

        System.out.println("=============================================================");
        System.out.println("->Check if a text contains a word");
        System.out.println("=============================================================");

        String text = "This is a new text, which contains a magic word";

        if(text.matches(".*magic.*")) {
            System.out.println("The text contains the magic word");
        }


        System.out.println("=============================================================");
        System.out.println("->Print out all words starting with capital letters");
        System.out.println("=============================================================");

        text = "This is an Example text where Some words start With Capital letters";

        Pattern p = Pattern.compile("\\b[A-Z]\\w+");
        Matcher m = p.matcher(text);

        int i = 0;
        while(m.find()) {
            System.out.println(i + ": " + m.group());
            i++;
        }

        System.out.println("=============================================================");
        System.out.println("->Check if a text starts with decimal");
        System.out.println("=============================================================");

        text = "66 is a long road";
        System.out.println(text.matches("\\d.*"));

        System.out.println("=============================================================");
        System.out.println("->Find and print out the first word of a text");
        System.out.println("=============================================================");

        text = "FirstWord is always the first word";

        p = Pattern.compile("^\\w+");
        m = p.matcher(text);
        m.find();

        System.out.println(m.group());

        System.out.println("=============================================================");
        System.out.println("->Find and print out the last word of a text");
        System.out.println("=============================================================");

        text = "The last word of this text is LastWord";

        p = Pattern.compile("\\w+$");
        m = p.matcher(text);

        m.find();

        System.out.println(m.group());

        System.out.println("=============================================================");
        System.out.println("->Use capturing groups");
        System.out.println("=============================================================");

        text = "Italy to America is popular but Honolulu to Barcelona is much cheaper";

        p = Pattern.compile("(\\w+)\\sto\\s(\\w+)");
        m = p.matcher(text);

        while(m.find()) {
            System.out.println("Source: " + m.group(1) + ", destination: " + m.group(2));
        }

        System.out.println("=============================================================");
        System.out.println("->Use named capturing groups");
        System.out.println("=============================================================");

        text = "Italy to America is popular but Honolulu to Barcelona is much cheaper";

        p = Pattern.compile("(?<source>\\w+)\\sto\\s(?<destination>\\w+)");
        m = p.matcher(text);

        while(m.find()) {
            System.out.println("Source: " + m.group("source") + ", destination: " + m.group("destination"));
        }

        System.out.println("=============================================================");
        System.out.println("->Use non-capturing groups");
        System.out.println("=============================================================");

        text = "Nowadays there are errors in the code some of them causing exceptions while some of them are normal errors";

        p = Pattern.compile("(?:errors|exceptions)");
        m = p.matcher(text);

        while(m.find()) {
            System.out.println(m.group() + ", start pos: " + m.start() + ", end pos: " + m.end());
        }

        System.out.println("=============================================================");
        System.out.println("->Use multiline flag, match the first number in each row");
        System.out.println("=============================================================");

        text = "This is line 7 instead of 5\nWhile 22 is a great number 7 or 6\nis still better than 3 or 9";

        p = Pattern.compile("(?m)^.*?(\\d)");
        m = p.matcher(text);

        while(m.find()) {
            System.out.println(m.group(1));
        }

        System.out.println("=============================================================");
        System.out.println("->Use case insensitivity mode");
        System.out.println("=============================================================");

        text = "WoRd is a good WOrD even with WorD or WORD";

        p = Pattern.compile("(?i)word");
        m = p.matcher(text);

        while (m.find()) {
            System.out.println(m.group());
        }

        System.out.println("=============================================================");
        System.out.println("->Use back reference");
        System.out.println("=============================================================");

        text = "gold-silver, metal-metal, coal-coal, steal-wood";

        p = Pattern.compile("(\\w+?)-\\1");
        m = p.matcher(text);

        while(m.find()) {
            System.out.println(m.group());
        }

        System.out.println("=============================================================");
        System.out.println("->Use back reference with named groups");
        System.out.println("=============================================================");

        text = "gold-silver, metal-metal, coal-coal, steal-wood";

        p = Pattern.compile("(?<firstWord>\\w+?)-\\k<firstWord>");
        m = p.matcher(text);

        while (m.find()) {
            System.out.println(m.group());
        }

        System.out.println("=============================================================");
        System.out.println("->Use replaceFirst");
        System.out.println("=============================================================");

        text = "This is a new text";

        System.out.println(text.replaceFirst("\\s", ""));

        System.out.println("=============================================================");
        System.out.println("->Use split");
        System.out.println("=============================================================");

        text = "This text contains several sentences. Really? I don't think so! I think this is a simple text.";

        String marks = Pattern.quote("?.!");
        String[] sentences = text.split("[" + marks + "]");

        for (int j = 0; j < sentences.length; j++) {
            System.out.println(sentences[j].trim());
        }

        System.out.println("=============================================================");
        System.out.println("->Use replacement reference");
        System.out.println("=============================================================");

        text = "This is a super text with super content";

        System.out.println(text.replaceAll("\\b(\\w+)\\b", "($1)"));

        
    }
}
