public class Main {

    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        tree.insert("N1");
        tree.insert("N2");
        tree.insert("N3");
        tree.insert("N4");
        tree.insert("N5");
        tree.insert("N6");
        tree.insert("N7");
        tree.insert("N8");
        tree.insert("N9");

        System.out.println("===================================================");
        System.out.println("-> Preorder traversal");
        System.out.println("===================================================");
        tree.preOrder(tree.root);
        System.out.println();
        System.out.println("===================================================");
        System.out.println("-> Inorder traversal");
        System.out.println("===================================================");
        tree.inOrder(tree.root);
        System.out.println();
        System.out.println("===================================================");
        System.out.println("-> Postorder traversal");
        System.out.println("===================================================");
        tree.postOrder(tree.root);
        System.out.println();
        System.out.println("===================================================");
        System.out.println("-> Level traversal");
        System.out.println("===================================================");
        tree.levelOrder();
        System.out.println();
        System.out.println("===================================================");
        System.out.println("-> Search");
        System.out.println("===================================================");
        tree.search("N9");
        tree.search("N10");
    }
}
