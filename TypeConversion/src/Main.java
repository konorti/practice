import java.nio.charset.StandardCharsets;

public class Main {

    static byte byteValue;
    static short shortValue;
    static int intValue;
    static long longValue;
    static float floatValue;
    static double doubleValue;
    static char c;
    static String s;

    public static void main(String[] args) {
        System.out.println("=============Primitive data types=============");
        System.out.println("boolean - 1 bit");
        System.out.println("byte - 1 byte");
        System.out.println("short - 2 byte");
        System.out.println("char - 2 byte");
        System.out.println("int - 4 byte");
        System.out.println("float - 4 byte");
        System.out.println("long - 8 byte");
        System.out.println("double - 8 byte");
        System.out.println("=============Ranges=============");
        System.out.println("byte: " + Byte.MIN_VALUE + ", " + Byte.MAX_VALUE);
        System.out.println("short: " + Short.MIN_VALUE + ", " + Short.MAX_VALUE);
        System.out.println("integer: " + Integer.MIN_VALUE + ", " + Integer.MAX_VALUE);
        System.out.println("long: " + Long.MIN_VALUE + ", " + Long.MAX_VALUE);
        System.out.println("float: " + Float.MIN_VALUE + ", " + Float.MAX_VALUE);
        System.out.println("double: " + Double.MIN_VALUE + ", " + Double.MAX_VALUE);
        System.out.println("================================");
        System.out.println("Default values");

        System.out.println("byte default: " + byteValue);
        System.out.println("short default: " + shortValue);
        System.out.println("int default: " + intValue);
        System.out.println("long default: " + longValue);
        System.out.println("float default: " + floatValue);
        System.out.println("double default: " + doubleValue);
        System.out.println("char default: " + c);
        System.out.println("String default: " + s);

        System.out.println("================================");
        System.out.println("Widening automatic conversion");

//        byte < short < int < long < float < double
        byteValue = 127;
        shortValue = byteValue;
        intValue = shortValue;
        longValue = intValue;
        floatValue = longValue;
        doubleValue = floatValue;

        System.out.println(byteValue);
        System.out.println(shortValue);
        System.out.println(longValue);
        System.out.println(floatValue);
        System.out.println(doubleValue);

        floatValue = Long.MAX_VALUE;
        doubleValue = Long.MAX_VALUE;
        System.out.println(Long.MAX_VALUE);
        System.out.println(floatValue);
        System.out.println(doubleValue);

        System.out.println("\t================================");
        System.out.println("\tConvert float to double");

        floatValue = 3.9E-44f;
        doubleValue = floatValue;
        System.out.println(doubleValue);

        System.out.println("================================");
        System.out.println("Narrowing explicit conversion");

        intValue = 300;
        byteValue = (byte)intValue;
        System.out.println(byteValue);

        intValue = 100;
        byteValue = (byte)intValue;
        System.out.println(byteValue);

        longValue = 3147483647L;
        intValue = (int)longValue;
        System.out.println(intValue);

        System.out.println("\t================================");
        System.out.println("\tConvert floating to integer");

        floatValue = 29.99f;
        intValue = (int)floatValue;
        System.out.println(intValue);

        doubleValue = 29.99d;
        intValue = (int)doubleValue;
        System.out.println(intValue);

        System.out.println("\t================================");
        System.out.println("\tRounding");

        floatValue = 1.1f;
        floatValue = Math.round(floatValue);
        System.out.println(floatValue);

        floatValue = 1.7f;
        floatValue = Math.round(floatValue);
        System.out.println(floatValue);

        floatValue = 1.1f;
        floatValue = (float)Math.ceil(floatValue);
        System.out.println(floatValue);

        floatValue = 1.7f;
        floatValue = (float)Math.floor(floatValue);
        System.out.println(floatValue);

        System.out.println("\t================================");
        System.out.println("\tConvert double to float");

        doubleValue = 3.9E-44d;
        System.out.println(doubleValue);
        floatValue = (float)doubleValue;

        System.out.println(floatValue);

        System.out.println("\t================================");
        System.out.println("\tConvert char to String");

        c = 'T';
        s = Character.toString(c);

        System.out.println(s);

        System.out.println("\t================================");
        System.out.println("\tConvert string to char");

        s = "T";
        c = s.charAt(0);

        System.out.println(c);

        System.out.println("\t================================");
        System.out.println("\tParse string to float");

        s = "2.4598";

        floatValue = Float.parseFloat(s);

        System.out.println(s);

        System.out.println("\t================================");
        System.out.println("\tConvert string to charArray");

        s = "String value to char array";
        char[] chars = s.toCharArray();
        System.out.println(chars);

        System.out.println("\t================================");
        System.out.println("\tConvert charArray to String");

        s = String.valueOf(chars);

        System.out.println(s);

        System.out.println("\t================================");
        System.out.println("\tConvert a String to byteArray");

        s = "New char value";

        byte[] byteArray = s.getBytes(StandardCharsets.UTF_16);

        System.out.println("\t================================");
        System.out.println("\tConvert a byteArray to String");

        String newString = new String(byteArray, StandardCharsets.UTF_16);
        System.out.println(newString);
    }
}
