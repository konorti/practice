package com.konorti.spring.core.factory.instancefactory;

public class FileReader implements Reader {

    @Override
    public void read() {
        System.out.println("File reader reads.");
    }
}
