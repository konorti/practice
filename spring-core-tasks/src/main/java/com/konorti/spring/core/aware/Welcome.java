package com.konorti.spring.core.aware;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import java.util.Locale;

public class Welcome implements MessageSourceAware {

    @Override
    public void setMessageSource(MessageSource messageSource) {
        System.out.println(messageSource.getMessage("welcome", null, Locale.ENGLISH));
        System.out.println(messageSource.getMessage("welcome", null, Locale.FRENCH));
        System.out.println(messageSource.getMessage("welcome", null, new Locale("hi", "IN")));
    }
}
