package com.konorti.spring.core.autowire.primary;

public class Car extends Vehicle {

    @Override
    public void drive() {
        System.out.println("Driving car");
    }
}
