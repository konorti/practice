package com.konorti.spring.core.aop.basic;

public interface BasicCalculator {

    /**
     * Adds two numbers
     *
     * @param number1
     * @param number2
     * @return
     */
    double addition(double number1, double number2);

    /**
     * Substract two numbers
     *
     * @param number1
     * @param number2
     * @return
     */
    double substraction(double number1, double number2);


    double multiplication(double number1, double number2);

    double division(double number1, double number2);
}
