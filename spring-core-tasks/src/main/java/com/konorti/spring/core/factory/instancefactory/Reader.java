package com.konorti.spring.core.factory.instancefactory;

public interface Reader {

    void read();
}
