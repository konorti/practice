package com.kn.practice;

import java.util.*;

public class LinkedListPractice {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();

        System.out.println("===================================================");
        System.out.println("-> Adding items to the list");
        System.out.println("===================================================");

        list.add(2);
        list.addFirst(1);
        list.add(3);
        list.addFirst(-1);
        list.add(1,0);
        list.addFirst(-2);

        System.out.println(list.contains(1));

        System.out.println("===================================================");
        System.out.println("-> Using iterator to loop a list");
        System.out.println("===================================================");
        {
            Iterator<Integer> iterator = list.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        }
        System.out.println("===================================================");
        System.out.println("-> Using iterator to remove elements from the list");
        System.out.println("===================================================");
        {
            Iterator<Integer> iterator = list.iterator();
            while (iterator.hasNext()) {
                if(iterator.next() < 0) {
                    iterator.remove();
                }
            }
            list.stream().forEach(System.out::println);
        }
        System.out.println("===================================================");
        System.out.println("-> Using listIterator to add elements to the list");
        System.out.println("===================================================");
        {
            ListIterator<Integer> iterator = list.listIterator();
            while (iterator.hasNext()) {
                iterator.add(iterator.next());
            }
            list.stream().forEach(System.out::println);
        }
        System.out.println("===================================================");
        System.out.println("-> Using listIterator to change elements to the list");
        System.out.println("===================================================");
        {
            ListIterator<Integer> iterator = list.listIterator();
            while (iterator.hasNext()) {
                iterator.set(iterator.next() * 10);
            }
            list.stream().forEach(System.out::println);
        }

        System.out.println("===================================================");
        System.out.println("-> Merge two lists, add a list to another");
        System.out.println("===================================================");
        {
            LinkedList<Integer> l1 = new LinkedList<>();
            LinkedList<Integer> l2 = new LinkedList<>();
            l1.add(1);
            l1.add(2);
            l2.add(30);
            l2.add(40);
            l1.addAll(l2);
            l1.stream().forEach(System.out::println);
            System.out.println("===");
            l1.addAll(0, l2);
            l1.stream().forEach(System.out::println);
        }
    }
}
