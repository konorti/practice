package com.kn.practice;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class QueuePractice {

    public static void main(String[] args) {

        System.out.println("===================================================");
        System.out.println("-> Main functionalities of a queue");
        System.out.println("-> FIFO - first in first out");
        System.out.println("-> Same as the collection + add to the end, remove/retrieve from the head (first) element.");
        System.out.println("===================================================");

        Queue<String> queue = new LinkedList<>();

        System.out.println("Add en element to the end of the queue without throwing an exception if there is not enough capacity.");
        queue.offer("element1");
        queue.offer("element2");
        queue.offer("element3");
        queue.offer("element4");
        System.out.println((Arrays.toString(queue.toArray())));

        System.out.println("Retrieve and remove the head of the queue. Throws an exception if the queue is empty.");
        System.out.println(queue.remove());
        System.out.println((Arrays.toString(queue.toArray())));

        System.out.println("Retrieve and remove the head of the queue. Returns null if the queue is empty.");
        System.out.println(queue.poll());
        System.out.println((Arrays.toString(queue.toArray())));

        System.out.println("Retrieve but NOT remove the head of the queue. Throws an exception if the queue is empty.");
        System.out.println(queue.element());
        System.out.println((Arrays.toString(queue.toArray())));

        System.out.println("Retrieve but NOT remove the head of the queue. Returns null if the queue is empty.");
        System.out.println(queue.peek());
        System.out.println((Arrays.toString(queue.toArray())));
    }
}
