package com.kn.practice;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class DequeuePractice {

    public static void main(String[] args) {

        System.out.println("===================================================");
        System.out.println("-> Main functionalities of a dequeue - Double ended queue");
        System.out.println("-> FIFO - first in first out, LIFO - last in first out");
        System.out.println("-> Same as the collection + queue + add to the head, remove/retrieve from the end (last) element.");
        System.out.println("===================================================");

        Deque<String> dequeue = new LinkedList<>();

        System.out.println("Add en element to the HEAD of the dequeue without throwing an exception if there is not enough capacity.");
        dequeue.addFirst("element6");
        dequeue.addFirst("element5");
        dequeue.addFirst("element4");
        System.out.println((Arrays.toString(dequeue.toArray())));

        System.out.println("Add en element to the HEAD of the dequeue. Throws an exception if there is not enough capacity.");
        dequeue.offerFirst("element3");
        dequeue.offerFirst("element2");
        dequeue.offerFirst("element1");
        System.out.println((Arrays.toString(dequeue.toArray())));

        System.out.println("Retrieve and remove from the end of the dequeue. Throws an exception if the dequeue is empty.");
        System.out.println(dequeue.removeLast());
        System.out.println((Arrays.toString(dequeue.toArray())));

        System.out.println("Retrieve and remove from the end of the dequeue. Returns null if the dequeue is empty.");
        System.out.println(dequeue.pollLast());
        System.out.println((Arrays.toString(dequeue.toArray())));

        System.out.println("Retrieve but NOT remove the end of the dequeue. Throws an exception if the dequeue is empty.");
        System.out.println(dequeue.getLast());
        System.out.println((Arrays.toString(dequeue.toArray())));

        System.out.println("Retrieve but NOT remove the end of the dequeue. Returns null if the dequeue is empty.");
        System.out.println(dequeue.peekLast());
        System.out.println((Arrays.toString(dequeue.toArray())));
    }
}
