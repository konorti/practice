package com.kn.practice;

import java.util.HashMap;
import java.util.Map;

public class HashMapPractice {
    public static void main(String[] args) {

        System.out.println("===================================================");
        System.out.println("-> Main functionalities of the HashMap");
        System.out.println("===================================================");

        HashMap<String, String> map = new HashMap<>();

        System.out.println("Add/replace a key-value pair to a map");
        map.put("ID1", "Data1");
        map.put("ID2", "Data2");
        System.out.println(map);

        System.out.println("Add a key-value pair to a map only if it is not mapped");
        map.putIfAbsent("ID7", "Data7");
        map.putIfAbsent("ID1", "New value");
        System.out.println(map);

        System.out.println("Add a map to another map");
        map.putAll(Map.of("ID3", "Data3", "ID4", "Data4"));
        System.out.println(map);

        System.out.println("Retrieve a value by key");
        System.out.println(map.get("ID3"));

        System.out.println("Retrieve a value by key or return the default if does not exist");
        System.out.println(map.getOrDefault("ID5", "Default"));

        System.out.println("Check if it contains a key");
        System.out.println(map.containsKey("ID4"));
        System.out.println(map.containsKey("ID5"));

        System.out.println("Check if it contains a value");
        System.out.println(map.containsValue("Data4"));
        System.out.println(map.containsValue("Data5"));

        System.out.println("Remove an entry by key");
        map.remove("ID4");
        System.out.println(map);

        System.out.println("Remove an entry by key only if it contains the given value");
        map.remove("ID2", "Data2");
        map.remove("ID1", "Data2");
        System.out.println(map);

        System.out.println("Replace a value which belongs to the provided key");
        map.replace("ID1", "Data1m");
        System.out.println(map);

        System.out.println("Replace a value which belongs to the provided key only if it contains the given value");
        map.replace("ID1", "WrongOldValue", "NewValue");
        map.replace("ID3", "Data3", "NewValue3");
        System.out.println(map);

        System.out.println("Change the values by executing a function on them");
        map.replaceAll((s, s2) -> s2.toUpperCase());
        System.out.println(map);

        System.out.println("Compute a value for the specified key using the provided function");
        map.compute("ID1", (s, s2) -> s2.toLowerCase());
        System.out.println(map);

        System.out.println("Compute a value for the specified key using the provided function only if it is not mapped or null");
        map.computeIfAbsent("ID1", s -> "INITIAL");
        map.computeIfAbsent("ID4", s -> "INITIAL");
        System.out.println(map);

        System.out.println("Compute a value for the specified key using the provided function only if it is mapped");
        map.computeIfPresent("ID5", (s, s2) -> s.concat(s2));
        map.computeIfPresent("ID1", (s, s2) -> s.concat(s2));
        System.out.println(map);

        System.out.println("Set the provided value if it is not mapped otherwise compute a new value");
        map.merge("ID8", "DEFAULT", (s, s2) -> s2.concat("-changed"));
        map.merge("ID7", "DEFAULT", (s, s2) -> s.concat("-changed"));
        System.out.println(map);

        System.out.println("Get the keys as a set");
        System.out.println(map.keySet());

        System.out.println("Get the values as a collection");
        System.out.println(map.values());

        System.out.println("Perform an action on all key value pairs like print them");
        map.forEach((key, value) -> System.out.println("Key: " + key + ", value: " + value));
    }
}
