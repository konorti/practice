package com.kn.practice;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

public class OwnIterableClass implements Iterable<String> {

    private static final int CAPACITY = 3;
    private LinkedBlockingQueue<String> store = new LinkedBlockingQueue<>(CAPACITY);

    public void add(String element) {
        if ( store.size() == CAPACITY ) {
            store.remove();
        }
        store.add(element);
    }

    @Override
    public Iterator<String> iterator() {
        return store.iterator();
    }

    @Override
    public void forEach(Consumer<? super String> action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator<String> spliterator() {
        return Iterable.super.spliterator();
    }

    public static void main(String[] args) {
        OwnIterableClass t = new OwnIterableClass();
        t.add("egy");
        t.add("ketto");
        t.add("harom");

        t.iterator().forEachRemaining(System.out::println);
        System.out.println("==");
        t.add("negy");
        t.iterator().forEachRemaining(System.out::println);
    }
}
