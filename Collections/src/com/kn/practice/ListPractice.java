package com.kn.practice;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ListPractice {

    public static void main(String[] args) {

        System.out.println("===================================================");
        System.out.println("-> Main functionalities of a list");
        System.out.println("-> Same as the collection + add/retrieve/remove/replace by index");
        System.out.println("===================================================");

        List<String> list = new LinkedList<>();

        System.out.println("Add en element to a specific index position.");
        list.add("element1");
        list.add("element3");
        list.add(1, "element2");
        System.out.println((Arrays.toString(list.toArray())));

        System.out.println("Add a collection to a specific index position.");
        list.addAll(2, List.of("element2.5", "element2.8"));
        System.out.println((Arrays.toString(list.toArray())));

        System.out.println("Remove an element from an index position.");
        list.remove(3);
        System.out.println((Arrays.toString(list.toArray())));

        System.out.println("Retrieve an element by providing an index position.");
        System.out.println(list.get(0));

        System.out.println("Replace an element by providing an index position.");
        list.set(1, "ELEMENT2");
        System.out.println((Arrays.toString(list.toArray())));

        System.out.println("Retrieve the index of an element.");
        System.out.println(list.indexOf("ELEMENT2"));

        System.out.println("Retrieve the last index of an element.");
        list.set(2, "ELEMENT2");
        System.out.println(list.lastIndexOf("ELEMENT2"));

        System.out.println("Return a subview of a list.");
        System.out.println(list.subList(1, 3));

        System.out.println("Replace all elements in the list.");
        list.subList(1, 3).replaceAll(e -> e +"_X");
        System.out.println((Arrays.toString(list.toArray())));

        System.out.println("Sort a list.");
        list.sort(Comparator.naturalOrder());
        System.out.println((Arrays.toString(list.toArray())));
        list.sort(Comparator.reverseOrder());
        System.out.println((Arrays.toString(list.toArray())));

        System.out.println("Return an unmodifiable list of a collection.");
        System.out.println(List.copyOf(list));
    }
}
