package com.kn.practice;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class CollectionPractice {

    public static void main(String[] args) {
        /*
         * Main functionalities of a collection
         * */

        System.out.println("===================================================");
        System.out.println("-> Main functionalities of a collection");
        System.out.println("===================================================");

        Collection<String> collection = new LinkedList<>();

        System.out.println("- Add an element");
        collection.add("element1");
        collection.stream().forEach(System.out::println);

        System.out.println("- Add another collection. Create the union of two collections.");
        collection.addAll(List.of("element2", "element3", "element4", "element5"));
        collection.stream().forEach(System.out::println);

        System.out.println("- Remove an element");
        collection.remove("element2");
        collection.stream().forEach(System.out::println);

        System.out.println("- Remove the elements of another collection");
        collection.removeAll(List.of("element3", "element4"));
        collection.stream().forEach(System.out::println);

        System.out.println("- Check if the collection contains an element");
        System.out.println(collection.contains("element1"));
        System.out.println(collection.contains("element2"));

        System.out.println("- Check if the collection contains the elements of another collection");
        System.out.println(collection.containsAll(List.of("element1", "element5")));
        System.out.println(collection.containsAll(List.of("element1", "element2")));

        System.out.println("- Create the intersection of two collections");
        collection.retainAll(List.of("element5"));
        collection.stream().forEach(System.out::println);

        System.out.println("- Remove all elements from the collection");
        collection.clear();
        collection.stream().forEach(System.out::println);

    }
}
