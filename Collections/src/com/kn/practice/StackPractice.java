package com.kn.practice;

import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingDeque;

public class StackPractice {
    public static void main(String[] args) {
        System.out.println("===================================================");
        System.out.println("-> Deque - Double Ended Queue can be used to implement a stack");
        System.out.println("-> Implementation 1 - Adding to the end removing from the end");
        System.out.println("===================================================");

        Deque<String> stackUnlimited = new LinkedList<>();

        //Using LinkedBlockingDeque we can assign capacity to the queue
        Deque<String> stackLimited = new LinkedBlockingDeque<>(2);

        // Offer and poll does not throw exception in case of full or empty queue
        stackLimited.offer("element1");
        stackLimited.offer("element2");
        stackLimited.offer("element3"); // This does not throw an exception just return false
        System.out.println(stackLimited.pollLast());
        System.out.println(stackLimited.pollLast());
        System.out.println(stackLimited.pollLast()); // This does not throw an exception just return null

        System.out.println("===");

        stackLimited.add("element1");
        stackLimited.add("element2");
//        stackLimited.add("element3"); // This would throw an exception
        System.out.println(stackLimited.removeLast());
        System.out.println(stackLimited.removeLast());
//        System.out.println(stackLimited.removeLast()); // This would throw an exception

        System.out.println("===================================================");
        System.out.println("-> Implementation 2 - Adding to the head removing from the head");
        System.out.println("===================================================");

        stackLimited.clear();
        stackLimited.addFirst("element1");
        stackLimited.addFirst("element2");
        System.out.println(stackLimited.remove());
        System.out.println(stackLimited.remove());


        System.out.println("===================================================");
        System.out.println("-> Implementation 3 - Using the stack methods of the queue");
        System.out.println("-> Adding to the head removing from the head");
        System.out.println("===================================================");

        stackLimited.clear();
        stackLimited.push("element1");
        stackLimited.push("element2");
        System.out.println(stackLimited.pop());
        System.out.println(stackLimited.pop());
    }
}
