package com.kn.practice;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {

//    How to define an array?
//    How to define a multi dimension array?
//    How to loop an array?
//    How to loop a multi dimension array?
//    How to get a random number from an array?
//    How to get a random number from a multi dimension array?
//    How to append a new item to an array?
//    How to check if an array is empty?
//    How to compare two arrays?
//    How to convert an array to a list?
//    How to convert a list to an array?
//    How to convert an array to a String?
//    How to merge two arrays?
//    Copy an array into a new array twice.
//    How to define and pass a new array in a method argument?
//    Write a method which prints out an array passed as a vararg argument.

    public static void main(String[] args) {

        String[] array = {
                "one", "two", "three"
        };

        String[][] multiArray = {
                {"01", "02", "03"},
                {"11", "12", "13"},
                {"21", "22", "23"},
        };

        System.out.println("================================");
        System.out.println("How to loop an array");

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        System.out.println("================================");
        System.out.println("How to loop a multi array");

        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                System.out.println(multiArray[i][j]);
            }
        }

        System.out.println("================================");
        System.out.println("How to get a random number from an array");

        Random random = new Random();
        System.out.println(array[random.nextInt(array.length)]);

        System.out.println("================================");
        System.out.println("How to get a random number from a multi array");

        System.out.println(multiArray[random.nextInt(multiArray.length)][random.nextInt(multiArray[0].length)]);

        System.out.println("================================");
        System.out.println("How to append a new item to an array");

        array = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = "new item";

        System.out.println(Arrays.toString(array));

        System.out.println("================================");
        System.out.println("How to check if an array is empty");

        String[] emptyArray = {};

        if (emptyArray == null || emptyArray.length == 0) {
            System.out.println("Empty array");
        }

        System.out.println("================================");
        System.out.println("How to compare two arrays");

        String[] array1 = {"one", "two", "three"};
        String[] array2 = {"one", "two", "three"};
        String[] array3 = {"one", "two", "four"};

        System.out.println(Arrays.equals(array1, array2));
        System.out.println(Arrays.equals(array1, array3));

        System.out.println("================================");
        System.out.println("How to convert an array to a list");

        List<String> l = Arrays.asList(array);

        System.out.println(Arrays.toString(l.toArray()));

        System.out.println("================================");
        System.out.println("How to convert a list to an array");

        String[] listArray = l.toArray(new String[0]);

        System.out.println(Arrays.toString(listArray));

        System.out.println("================================");
        System.out.println("How to convert an array to a String");

        System.out.println(Arrays.toString(array));

        System.out.println("================================");
        System.out.println("How to merge two arrays");

        Integer[] mArray1 = {1, 2, 3};
        Integer[] mArray2 = {4, 5, 6};

        Integer[] mergedArray = new Integer[mArray1.length + mArray2.length];

        System.arraycopy(mArray1, 0, mergedArray, 0, mArray1.length);
        System.arraycopy(mArray2, 0, mergedArray, mArray1.length, mArray2.length);

        System.out.println(Arrays.toString(mergedArray));

        System.out.println("================================");
        System.out.println("How to define and pass a new array in a method argument");

        System.out.println(Arrays.toString(new String[]{"a", "b", "c"}));

        System.out.println("================================");
        System.out.println("Write a method which prints out an array passed as a vararg argument");

        class VarargExample {
            public void printArray(Object ... array) {
                System.out.println(Arrays.toString(array));
            }
        }

        new VarargExample().printArray(2.34, 43.5, 434.4);
    }
}
