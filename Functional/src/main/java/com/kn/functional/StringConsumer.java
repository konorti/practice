package com.kn.functional;

import java.util.Queue;

public class StringConsumer extends AbstractConsumer<String> {

    public StringConsumer(Queue<String> queue, int id) {
        super(queue, id);
    }

    @Override
    public void consume() {
        String item = queue.poll();
        System.out.println("com.kn.functional.Consumer " + id + " consumed item: " + item + ", queue: " + queue);
    }
}
