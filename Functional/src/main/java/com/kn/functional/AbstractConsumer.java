package com.kn.functional;

import java.util.Queue;

public abstract class AbstractConsumer<T> implements Runnable {

    protected volatile Queue<T> queue;
    protected int id;

    public AbstractConsumer(Queue<T> queue, int id) {
        this.queue = queue;
        this.id = id;
    }

    @Override
    public void run() {
        while (true) {
            consume();
        }
    }

    public abstract void consume();
}
