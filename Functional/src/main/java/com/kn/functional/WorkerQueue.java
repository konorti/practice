package com.kn.functional;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class WorkerQueue<T> implements Queue<T> {
    private BlockingQueue<T> queue = new LinkedBlockingQueue<>();
    private int maxSize;
    private ReentrantLock consumerLock = new ReentrantLock(true);
    private ReentrantLock producerLock = new ReentrantLock(true);
    private Condition producerCondition = producerLock.newCondition();
    private Condition consumerCondition = consumerLock.newCondition();


    public WorkerQueue(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public boolean add(T item) {
        consumerLock.lock();
        producerLock.lock();
        if (queue.size() == maxSize) {
            try {
                consumerCondition.signalAll();
                consumerLock.unlock();
                System.out.println(Thread.currentThread().getId() + " producer starts waiting");
                producerCondition.await();
                System.out.println(Thread.currentThread().getId() + " producer has been awaken");
                add(item);
                return true;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
        queue.add(item);
        producerLock.unlock();
        consumerLock.unlock();
        return true;
    }

    @Override
    public T poll() {
        consumerLock.lock();
        producerLock.lock();
        if( queue.size() == 0 ) {
            try {
                producerCondition.signalAll();
                producerLock.unlock();
                System.out.println(Thread.currentThread().getId() + " consumer starts waiting");
                consumerCondition.await();
                System.out.println(Thread.currentThread().getId() + " consumer has been awaken");
                return poll();
            } catch (InterruptedException e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            }
        }
        T item = queue.poll();
        producerLock.unlock();
        consumerLock.unlock();
        return item;
    }

    @Override
    public int size() {
        return queue.size();
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return queue.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return queue.iterator();
    }

    @Override
    public Object[] toArray() {
        return queue.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return queue.toArray(a);
    }

    @Override
    public boolean remove(Object o) {
        return queue.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return queue.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return queue.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return queue.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return queue.retainAll(c);
    }

    @Override
    public void clear() {
        queue.clear();
    }

    @Override
    public boolean equals(Object o) {
        return queue.equals(o);
    }

    @Override
    public int hashCode() {
        return queue.hashCode();
    }

    @Override
    public boolean offer(T t) {
        return queue.offer(t);
    }

    @Override
    public T element() {
        return queue.element();
    }

    @Override
    public T peek() {
        return queue.peek();
    }

    @Override
    public String toString() {
        return this.queue.toString();
    }

    @Override
    public T remove() {
        return queue.remove();
    }
}
