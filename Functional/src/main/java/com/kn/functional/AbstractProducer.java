package com.kn.functional;

import java.util.Queue;
import java.util.concurrent.TimeUnit;

abstract class AbstractProducer<T> implements Runnable {

    protected volatile Queue<T> queue;
    protected int id;

    public AbstractProducer(Queue<T> queue, int id) {
        this.queue = queue;
        this.id = id;
    }

    @Override
    public void run() {
        while (true) {
            produce();
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public abstract void produce();
}