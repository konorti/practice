package com.kn.functional;

import java.util.Queue;

public class NumberProducer extends AbstractProducer<String> {

    public NumberProducer(Queue<String> queue, int id) {
        super(queue, id);
    }

    @Override
    public void produce() {
        String item = String.valueOf((int)(Math.random() * 100));
        queue.add(item);
        System.out.println("com.kn.functional.Producer " + id + " produced the following item: " + item + ", queue: " + queue);
    }
}
