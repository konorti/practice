package com.kn.functional;

import java.util.Queue;

public class SpecialCharacterProducer extends AbstractProducer<String> {

    private String alphabet = "*-!@#$%^&*()_+";

    public SpecialCharacterProducer(Queue<String> queue, int id) {
        super(queue, id);
    }

    @Override
    public void produce() {
        String item = String.valueOf(alphabet.charAt((int)(Math.random() * alphabet.length())));
        queue.add(item);
        System.out.println("com.kn.functional.Producer " + id + " produced the following item: " + item + ", queue: " + queue);
    }
}
