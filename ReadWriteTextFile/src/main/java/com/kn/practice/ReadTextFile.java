package com.kn.practice;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class ReadTextFile {

    public static void main(String[] args) {

        /**
         * Important classes:
         *   - FileInputStream
         *      - A FileInputStream obtains input bytes from a file in a file system.
         *      - FileInputStream is meant for reading streams of raw bytes such as image data.
         *   - FileReader
         *      - Reads text from character files using a default buffer size.
         *      - Decoding from bytes to characters uses either a specified charset or the
         *        platform's default charset.
         *   - BufferedReader
         *      - Reads text from a character-input stream, buffering characters so as to provide
         *        for the efficient reading of characters, arrays, and lines.
         */

        System.out.println("\n========================================");
        System.out.println("Read a text file using FileInputStream");
        System.out.println("Working Directory = " + System.getProperty("user.dir"));

        try (InputStream inputStream = new FileInputStream("file.txt")) {
            int data;

            while ((data = inputStream.read()) != -1) {
                System.out.print((char) data);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n========================================");
        System.out.println("Read a text file using InputStream with readAllBytes");

        try (FileInputStream inputStream = new FileInputStream("file.txt")) {
            System.out.print(new String(inputStream.readAllBytes(), StandardCharsets.UTF_8));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n========================================");
        System.out.println("Read a text file using FileReader");

        try (InputStreamReader inputStreamReader = new FileReader("file.txt",
                StandardCharsets.UTF_8)) {

            char[] buffer = new char[100];

            int datasize;
            while ((datasize = inputStreamReader.read(buffer)) != -1) {
                System.out.print(String.valueOf(buffer, 0, datasize));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n========================================");
        System.out.println("Read a text file using BufferedReader");

        try (BufferedReader bufferedReader =
                new BufferedReader(new FileReader("file.txt", StandardCharsets.UTF_8))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n========================================");
        System.out.println("Read a text file using ByteArrayOutputStream");

        try (InputStream inputStream = new FileInputStream("file.txt");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {

            byte[] buffer = new byte[1024 * 4];

            while ((inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer);
            }

            System.out.println(byteArrayOutputStream.toString(StandardCharsets.UTF_8));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n========================================");
        System.out.println("Read a text file using a scanner");

        try (Scanner scanner = new Scanner(Path.of("file.txt"), StandardCharsets.UTF_8)) {

            while (scanner.hasNext()) {
                System.out.println(scanner.nextLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
