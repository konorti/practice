package com.kn.practice;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class WriteTextFile {
    public static void main(String[] args) {

        System.out.println("\n========================================");
        System.out.println("Write a text file using OutputStream");

        try(OutputStream outputStream = new FileOutputStream("outputStream.txt")) {
            String data = "This is a new String file\nWe plan to create";
            outputStream.write(data.getBytes(StandardCharsets.UTF_16));
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println("\n========================================");
        System.out.println("Write a text file using OutputStreamWriter");

        try(OutputStream outputStream = new FileOutputStream("OutputStreamWriter.txt");
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8)) {
            String data = "This text has been written into file\nUsing an OutputStreamWriter";

            outputStreamWriter.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n========================================");
        System.out.println("Write a text file using BufferedWriter");

        try(BufferedWriter bufferedWriter =
                    Files.newBufferedWriter(Paths.get("BufferedWriter.txt"),StandardCharsets.UTF_8)) {
            String data = "This text has been written into file\nUsing a BufferedWriter";
            bufferedWriter.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
