package com.kn.practice.classpractice;

public class ChildClass extends ParentClass {
    static String parentStaticVariable = "childStaticVariable";

    static {
        System.out.println("ChildClass static block using " + parentStaticVariable);
    }

    ChildClass() {
        System.out.println("ChildClass constructor");
    }

    // Static methods are technically not overridden just it hides the parent's implementation
    static void parentClassStaticMethod() {
        System.out.println("parentClassStaticMethod hidden from the childClass");
    }

//    Final methods cannot be overwritten
//
//    final void parentClassFinalMethod() {
//        System.out.println("parentClassFinalMethod");
//    }
//
//    final static void parentClassFinalStaticMethod() {
//        System.out.println("parentClassFinalStaticMethod");
//    }

    @Override
    void parentClassMethod() {
        System.out.println("parentClassMethod overriden in childclass");
    }
}
