package com.kn.practice.classpractice;

import com.kn.practice.classpractice.ClassWithNestedClass.InnerClass;
import com.kn.practice.classpractice.ClassWithNestedClass.StaticNestedClass;

public class NestedClassPractice {

    public static void main(String[] args) {
        ClassWithNestedClass classWithNestedClass = new ClassWithNestedClass();
        InnerClass innerClass = classWithNestedClass. new InnerClass();
        System.out.println(innerClass.publicInnerClassField);
        System.out.println(innerClass.accessPrivateField);
        StaticNestedClass staticNestedClass = new StaticNestedClass();
    }
}
