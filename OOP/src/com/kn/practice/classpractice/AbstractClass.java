package com.kn.practice.classpractice;

public abstract class AbstractClass {

    String privateAbstractVariable = "privateAbstractVariable";

    abstract void abstractMethod();

    void abstractClassMethod() {
        System.out.println("Abstract class method");
    }
}
