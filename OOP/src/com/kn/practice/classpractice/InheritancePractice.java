package com.kn.practice.classpractice;

public class InheritancePractice {
    public static void main(String[] args) {
        ChildClass childClass = new ChildClass();
        childClass.parentClassMethod();
        ChildClass.parentClassStaticMethod();
        useAbstractClass(new AbstractClass() {
            @Override
            void abstractMethod() {
                System.out.println(privateAbstractVariable);
                System.out.println("This is an anonymous class extending the AbstractClass");
            }
        });
    }

    public static void useAbstractClass(AbstractClass a) {
        a.abstractMethod();
    }
}
