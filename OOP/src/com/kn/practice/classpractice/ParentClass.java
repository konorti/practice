package com.kn.practice.classpractice;

public class ParentClass {
    static String parentStaticVariable = "parentStaticVariable";

    final String parentFinalVariable = "parentFinalVariable";
    String parentPackagePrivateVariable = "parentPackagePrivateVariable";
    protected String parentProtectedVariable = "parentProtectedVariable";
    private String parentPrivateVariable = "parentPrivateVariable";

    static {
        System.out.println("ParentClass static block");
    }

    ParentClass() {
        System.out.println("ParentClass constructor");
    }

    static void parentClassStaticMethod() {
        System.out.println("parentClassStaticMethod");
    }

    final void parentClassFinalMethod() {
        System.out.println("parentClassFinalMethod");
    }

    final static void parentClassFinalStaticMethod() {
        System.out.println("parentClassFinalStaticMethod");
    }

    void parentClassMethod() {
        System.out.println("parentClassMethod");
    }
}
