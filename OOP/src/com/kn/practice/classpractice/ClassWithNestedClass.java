package com.kn.practice.classpractice;

public class ClassWithNestedClass {

    private String privateOuterClassField = "privateOuterClassField";
    String outerClassField = "outerClassField";

    ClassWithNestedClass() {
        System.out.println(new PrivateInnerClass().publicInnerClassField);
        System.out.println(new PrivateInnerClass().privateInnerClassField);
    }

    // Use static nested class and only use inner class if it is really necessary
    private class PrivateInnerClass {
        // The outer class can access even the private members
        private String privateInnerClassField = "privateInnerClassField";
        public String publicInnerClassField = "publicInnerClassField";
    }

    class InnerClass {
        private String privateInnerClassField = "privateInnerClassField";
        public String publicInnerClassField = "publicInnerClassField";
        // Inner class has access to private fields of the outer class
        public String accessPrivateField = privateOuterClassField;

    }
// A static nested class interacts with the instance members of its outer class just like any other
// top-level class. In effect, a static nested class is behaviorally a top-level class that has been
// nested in another top-level class for packaging convenience.
    static class StaticNestedClass {
        StaticNestedClass() {
            System.out.println("StaticNestedClass");
//            Cannot access the members of the outer class
//            System.out.println(outerClassField);
        }
    }
}
