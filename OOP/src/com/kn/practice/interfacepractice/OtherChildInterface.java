package com.kn.practice.interfacepractice;

public interface OtherChildInterface extends ParentInterface {

    void noDefMethodOtherChild(); // Public and abstract by default. Only public is allowed.

}
