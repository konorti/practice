package com.kn.practice.interfacepractice;

public interface ParentInterface {
    String GREETING = "Hello from ParentInterface"; // All fields are public and static by default. Only public is allowed.

    static void staticPublicMethod(){
        System.out.println("staticPublicMethod in the ParentInterface");
    }

    private static void staticPrivateParentMethod(){
        System.out.println("staticPrivateParentMethod in the ParentInterface");
    }

    void noDefMethodParent(); // Public and abstract by default. Only public is allowed.

    default void methodWithDefaultImpl() { // default method does not need to be overridden.
        System.out.println(GREETING);
    }
}
