package com.kn.practice.interfacepractice;

public interface ChildInterface extends ParentInterface {

    String GREETING = "Hello from ChildInterface"; // All fields are public and static by default. Only public is allowed.

    static void staticPublicMethod(){
        System.out.println("staticPublicMethod in the ChildInterface");
    }

    void noDefMethodChild(); // Public and abstract by default. Only public is allowed.

    default void methodWithDefaultImpl() { // default method does not need to be overridden.
        System.out.println(GREETING);
    }
}
