package com.kn.practice.interfacepractice;

public class InterfaceImplementation implements ChildInterface, OtherChildInterface {

    /*
    * A class can implement multiple interfaces.
    * An interface can extend another interface.
    * All method in an interface is public and abstract by default.
    * An interface can have static public variables. Static variables cannot be private.
    * An interface can have static methods providing implementation. Static methods can be private.
    * A method can have default implementation using the default keyword.
    * */

    @Override
    public void noDefMethodChild() {

    }

    @Override
    public void noDefMethodParent() {

    }

    @Override
    public void noDefMethodOtherChild() {

    }

    public static void main(String[] args) {
        InterfaceImplementation i = new InterfaceImplementation();
        i.methodWithDefaultImpl();
        ParentInterface.staticPublicMethod();
        ChildInterface.staticPublicMethod();
    }
}
