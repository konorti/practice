package com.kn.practice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class Main {

    public static void main(String[] args) {

        /*
         * Important classes
         * - Paths - Consists exclusively of static methods that return a Path by converting a path string or URI.
         * - Files - Consists exclusively of static methods that operate on files, directories, or other types of files.
         * */

        System.out.println("===================================================");
        System.out.println("-> Check if file exists");
        System.out.println("===================================================");

        Path p = Paths.get("data.txt");
        Path p2 = Paths.get("notExist.txt");

        System.out.println(Files.exists(p));
        System.out.println(Files.exists(p2));

        System.out.println("===================================================");
        System.out.println("-> Check if file or directory");
        System.out.println("===================================================");

        Path itIsAFile = Paths.get("data.txt");
        Path itIsADirectory = Paths.get("..\\");

        System.out.println(Files.isRegularFile(itIsAFile));
        System.out.println(Files.isRegularFile(itIsADirectory));
        System.out.println(Files.isDirectory(itIsAFile));
        System.out.println(Files.isDirectory(itIsADirectory));

        System.out.println("===================================================");
        System.out.println("-> Get file length");
        System.out.println("===================================================");

        Path fileLength = Paths.get("data.txt");

        try {
            System.out.println(Files.size(fileLength));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> List available system roots");
        System.out.println("===================================================");

        Iterable<Path> roots = FileSystems.getDefault().getRootDirectories();

        for (Path root : roots) {
            System.out.println(root);
        }

        System.out.println("===================================================");
        System.out.println("-> List the content of a directory");
        System.out.println("===================================================");

        Path directory = Paths.get(".");

        try {
            Files.list(directory)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> Create, rename and delete a file");
        System.out.println("===================================================");

        Path crdFile = Paths.get("crd.file");
        Path crdNewFile = Paths.get("crd.new");

        try {
            Files.createFile(crdFile);
            System.out.println(Files.exists(crdFile));

            Files.move(crdFile, crdNewFile);
            System.out.println(Files.exists(crdFile));
            System.out.println(Files.exists(crdNewFile));

            Files.delete(crdNewFile);
            System.out.println(Files.exists(crdNewFile));

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> Create, rename and delete a directory");
        System.out.println("===================================================");

        directory = Paths.get("newDir");
        Path newDirectory = Paths.get("newDirV2");

        try {
            Files.createDirectory(directory);

            System.out.println(Files.exists(directory));

            Files.move(directory, newDirectory);

            System.out.println(Files.exists(directory));
            System.out.println(Files.exists(newDirectory));

            Files.delete(newDirectory);

            System.out.println(Files.exists(newDirectory));

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> Print out the folders from a path");
        System.out.println("===================================================");
        Path userHome = Paths.get(System.getProperty("user.home"));

        System.out.println(userHome);

        for (int i = 0; i < userHome.getNameCount(); i++) {
            System.out.println(userHome.getName(i));
        }

        System.out.println("===================================================");
        System.out.println("-> Create multiple directories from a path");
        System.out.println("===================================================");

        directory = Paths.get("new/folder/created");

        try {
            Files.createDirectories(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> Delete multiple directories from a path");
        System.out.println("===================================================");

        try {
            directory = Paths.get("new/folder/created");

            Files.walkFileTree(directory.getName(0), new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                        throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> Join two paths");
        System.out.println("===================================================");

        Path p1 = Paths.get("user");
        System.out.println(p1.resolve("lib"));

        System.out.println("===================================================");
        System.out.println("-> Copy file");
        System.out.println("===================================================");

        Path copiedFile = Paths.get("file.data");
        Path newFile = Paths.get("file.copy");

        try {
            Files.createFile(copiedFile);
            Files.copy(copiedFile, newFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> Copy a folder recursively");
        System.out.println("===================================================");

        Path folderStruct = Paths.get("folder1/folder2/folder3");
        Path rfile1 = Paths.get("folder1/file1.r");
        Path rfile2 = Paths.get("folder1/folder2/file2.r");
        Path rfile3 = Paths.get("folder1/folder2/folder3/file3.r");
        Path toCopyFolder = Paths.get("folderNew");

        try {
            Files.createDirectories(folderStruct);
            Files.createFile(rfile1);
            Files.createFile(rfile2);
            Files.createFile(rfile3);

            final Path frCopyFolder = folderStruct.getName(0);

            Files.walkFileTree(frCopyFolder, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                        throws IOException {
                    Files.copy(dir, toCopyFolder.resolve(frCopyFolder.relativize(dir)));
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                        throws IOException {
                    Files.copy(file, toCopyFolder.resolve(frCopyFolder.relativize(file)));
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> Write text into a file");
        System.out.println("===================================================");

        try (BufferedWriter writer = Files.newBufferedWriter(Path.of("fileWithContent"))) {
            writer.write("Hello World\n");
            writer.write("This is a good day\n");
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("===================================================");
        System.out.println("-> Read text from a file");
        System.out.println("===================================================");

        try(BufferedReader reader = Files.newBufferedReader(Path.of("fileWithContent"))) {
            String line;
            while((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            Files.delete(Path.of("fileWithContent"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

