package com.kn.practice;

import java.io.Serializable;

public class SerializableClass implements Serializable {
    private static final long serialVersionUID = 1L; // This indicates the version of the file.

    private String serializableVariable;
    private transient String notSerializableVariable;

    public SerializableClass(String serializableVariable, String notSerializableVariable) {
        this.serializableVariable = serializableVariable;
        this.notSerializableVariable = notSerializableVariable;
    }

    public String getSerializableVariable() {
        return serializableVariable;
    }

    public String getNotSerializableVariable() {
        return notSerializableVariable;
    }
}
