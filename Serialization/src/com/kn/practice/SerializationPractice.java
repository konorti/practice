package com.kn.practice;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class SerializationPractice {

    /*
    * A class have to implement the Serializable interface.
    * The Serializable interface does not contain anything it is only a marker.
    * Fields with transient keyword are not serialized.
    * ObjectOutputStream, ObjectInputStream are used to save and retrieved serialized object graphs.
    * The serialVersionUID indicates the version of the file. If the serialized object and the class
    * have different serialVersionUID than the deserialization will fail.
    * If there is an incompatible change in the structure of the class than the serialVersionUID
    * has to be changed.
    * If there is no serialVersionUID defined in the class than JVM will automatically generate one.
    * */

    public static void main(String[] args) {
        final String SERIALIZATION_TEST_FILE = "test_serialization.ser";

        try (FileOutputStream fileOutputStream = new FileOutputStream(SERIALIZATION_TEST_FILE)) {
            ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
            SerializableClass serializableClass = new SerializableClass("My serializable class",
                    "notSerialized");
            outputStream.writeObject(serializableClass);
            System.out.println("Object is serialized");
            try (FileInputStream fileInputStream = new FileInputStream(SERIALIZATION_TEST_FILE)) {
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                SerializableClass deserializedObject = (SerializableClass) objectInputStream.readObject();
                System.out.println(deserializedObject.getSerializableVariable());
                System.out.println(deserializedObject.getNotSerializableVariable());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Files.delete(Path.of(SERIALIZATION_TEST_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
