package com.kn.practice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPoolPractice {

    /*
    * Fixed sized threadpool. Can execute n threads the others are waiting in a queue.
    * */
    private static class MyTask implements Runnable{
        int i;
        MyTask(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            System.out.println("Task "+ i +" is running");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Task "+ i +" ended");
        }
    }
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 10; i++) {
            executorService.execute(new MyTask(i));
        }
    }
}
