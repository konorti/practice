package com.kn.practice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Thread.sleep;

public class StartingThreads {

    static class MyThread extends Thread {
        @Override
        public void run() {
            super.run();
            System.out.println("MyThread started");
            try {
                sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Mythread ended");
        }
    }

    static class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("Runnable started");
            try {
                sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Runnable ended");
        }
    }

    public static void main(String[] args) {
        MyThread t1 = new MyThread();
        Thread t2 = new Thread(() -> {
            System.out.println("T2 started");
            try {
                sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("T2 ended");
        });
        Thread t3 = new Thread(new MyRunnable());
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        t1.start();
        t2.start();
        t3.start();
        executorService.execute(() -> {
            System.out.println("Executor thread started");
            try {
                sleep(8000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Executor thread ended");
        });
    }
}
