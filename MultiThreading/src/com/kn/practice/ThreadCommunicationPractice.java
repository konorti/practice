package com.kn.practice;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadCommunicationPractice {

    private static Object lock = new Object();
    private static LinkedList<Integer> stack = new LinkedList();

    // Threads can communicate using the wait() notify() methods from synchronized methods and blocks.
    // The thread wich acquired the lock in the block can call the wait() method. In this case the thread
    // releases the lock and waits till the other thread calls notify() and leave the block and release the lock.
    public static void main(String[] args) {

        ExecutorService executor = Executors.newFixedThreadPool(2);

        executor.execute(() -> {
            for (int i = 0; i < 3; i++) {
                try {
                    sendMessage("Thread1", i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void sendMessage(String thread, int i) throws InterruptedException {
        synchronized (lock) {
            stack.push(i);
            System.out.println(thread + ": Message " + i + " sent.");
            System.out.println(thread + ": Waiting for message to be consumed");
            lock.wait();
            System.out.println(thread + ": Is active again.");
        }
    }

    private static void consumeMessage(String thread) throws InterruptedException {
        synchronized (lock) {
            if (!stack.isEmpty()) {
                System.out.println(thread + ": Message " + stack.pop() + " consumed.");
                lock.notify();
                System.out.println(thread + ": Waiting thread is notified.");
                System.out.println(thread + ": Leaving the synchronized block.");
            }
        }
    }
}
