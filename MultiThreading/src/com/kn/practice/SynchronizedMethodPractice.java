package com.kn.practice;

public class SynchronizedMethodPractice {

    private int counter;

    // Only one thread can execute the method in the same time
    synchronized void incCounter() {
        counter++;
    }

    int getCounter() {
       return counter;
    }

    public static void main(String[] args) {
        SynchronizedMethodPractice obj = new SynchronizedMethodPractice();
        System.out.println("\n========================================");
        System.out.println("Using synchronized method");
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                obj.incCounter();
                System.out.println("Thread1: " + obj.getCounter());
            }
        }).start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                obj.incCounter();
                System.out.println("Thread2: " + obj.getCounter());
            }
        }).start();
    }

}
