package com.kn.practice;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {

        System.out.println("\n========================================");
        System.out.println("Which are the important functional interfaces in java?");

        System.out.printf("%nImportant functional interfaces:%n" +
                "-Consumer<T>(T) void%n" +
                "-Supplier<T>() return T%n" +
                "-Predicate<T>(T) return Boolean%n" +
                "-UnaryOperator<T>(T) return T%n" +
                "-BinaryOperator<T>(T,T) return T%n" +
                "-Function<T,R>(T) return R%n");

        System.out.println("\n========================================");
        System.out.println("From a string list print out only those elements " +
                "which do not contain the 'error' literal.");

        List<String> list = List.of("Not reproducible", "Low error", "Consultancy", "High error");

        list.stream()
            .filter((element) -> element.matches(".*error.*"))
            .forEach(System.out::println);

        System.out.println("\n========================================");
        System.out.println("Print out all the elements from a list which contains list of strings.");

        List<List<String>> listContainer = List.of(
                List.of("11", "12"),
                List.of("21", "22"),
                List.of("31", "32")
        );

        listContainer.stream()
                .flatMap((element) -> element.stream())
                .forEach(System.out::println);

        System.out.println("\n========================================");
        System.out.println("Check if a list of strings contains a string, " +
                "if yes print out 'founded' otherwise 'not found'.");

        List<String> stringList = List.of("Apple", "Pear", "Cherry");

        Optional<String> match = stringList.stream()
            .filter((s) -> s.matches("Apple"))
            .findFirst();

        System.out.println(match.isPresent() ? "founded" : "not found");

        System.out.println("\n========================================");
        System.out.println("Convert all elements of a list to uppercase.");

        List<String> stringListToUpperCase = List.of("first", "second", "third");

        stringListToUpperCase = stringListToUpperCase.stream()
            .map(String::toUpperCase)
            .collect(Collectors.toList());

        stringListToUpperCase.stream()
            .forEach(System.out::println);

        System.out.println("\n========================================");
        System.out.println("Get the max element from a list.");

        List<Integer> numbers = List.of(1, 2, 3, 4, 5);

        Integer maxNumber = numbers.stream()
            .max(Integer::compare)
            .get();

        System.out.println(maxNumber);

        System.out.println("\n========================================");
        System.out.println("What are the elements of the reduce operation?");

        System.out.println("Element of reduce operation: identity, accumulator, combiner");

        System.out.println("\n========================================");
        System.out.println("Get the sum of a list using reduce.");

        Integer sum = numbers.stream()
                .reduce(0, Integer::sum);

        System.out.println(sum);

        System.out.println("\n========================================");
        System.out.println("Split a list into two lists one whose elements contains digits " +
                "and one whose elements don't.");

        list = List.of("this", "is", "word2", "hover", "hover2");

        Map<Boolean, List<String>> result = list.stream()
                .collect(Collectors.partitioningBy((element) -> element.matches(".*\\d.*")));

        System.out.println("Contains digit: " + result.get(true));
        System.out.println("Does not contain digit: " + result.get(false));

        System.out.println("\n========================================");
        System.out.println("Collect the words of a text into separate lists " +
                "according to the length of the word.");

        String text = "This is an example sentence what we will use today to test this example";

        Map<Integer, List<String>> groups = Arrays.stream(text.split(" "))
            .collect(Collectors.groupingBy(String::length));

        groups.keySet().stream()
            .forEach((wordLength) ->
                System.out.printf("Length: %d -> %s%n", wordLength, groups.get(wordLength)));

        System.out.println("\n========================================");
        System.out.println("Print out a list in the following format: {element1, element2, element3}");

        List<String> l = List.of("First", "Second", "Third");

        Collector<String, StringBuilder, String> MyListCollector = new Collector<>() {
            @Override
            public Supplier<StringBuilder> supplier() {
                return StringBuilder::new;
            }

            @Override
            public BiConsumer<StringBuilder, String> accumulator() {
                return (sb, text) -> {
                    if(sb.length() == 0) {
                        sb.append(text);
                    } else {
                        sb.append(", ").append(text);
                    }
                };
            }

            @Override
            public BinaryOperator<StringBuilder> combiner() {
                return (first, second) -> first.append(second.toString());
            }

            @Override
            public Function<StringBuilder, String> finisher() {
                return (sb) -> sb.replace(0,0,"{").append("}").toString();
            }

            @Override
            public Set<Characteristics> characteristics() {
                return EnumSet.of(Characteristics.UNORDERED);
            }
        };

        String r = l.stream()
            .collect(MyListCollector);

        System.out.println(r);

        System.out.println("\n========================================");
        r = l.stream()
            .collect(Collectors.joining(", ", "{", "}"));

        System.out.println(r);

        System.out.println("\n========================================");
        System.out.println("Print out the sum of the numbers from 0 to 100.");

        int sumNumbers = IntStream.range(0, 101)
            .reduce(Integer::sum).getAsInt();

        System.out.println(sumNumbers);

        System.out.println("\n========================================");
        System.out.println("From a list of string print out all the elements which starts with " +
                "'The' and give them back as a set.");

        l = List.of("word", "order", "The sentence", "The king", "The king");

        HashSet<String> collectedWords = l.stream()
            .filter((element)-> element.matches("The.*"))
            .collect(Collectors.toCollection(HashSet::new));

        collectedWords.stream()
            .forEach(System.out::println);
    }
}
