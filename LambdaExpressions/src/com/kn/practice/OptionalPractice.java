package com.kn.practice;

import java.util.Optional;

public class OptionalPractice {
    public static void main(String[] args) {
        Optional<Test> test = getTest(false);
        System.out.println("- Not empty optional");
        test.ifPresent(i -> System.out.println(i.name));
        System.out.println("- Empty optional");
        test = getTest(true);
        test.ifPresent(i -> System.out.println(i.name));
    }

    private static class Test {
        public Test(String name) {
            this.name = name;
        }

        public String name;
    }

    private static Optional<Test> getTest(boolean isNull) {
        if (isNull) {
            return Optional.empty();
        } else {
            return Optional.of(new Test("Hello"));
        }
    }
}
